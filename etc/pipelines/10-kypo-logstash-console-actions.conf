input {
    http {
        port => 9603
    }
}
filter {
    # filter messages on only messages which are from bash_history programname and contains KYPO_BASH_ACTION_AUDIT in the message
    if ("KYPO_BASH_ACTION_AUDIT " in [message]) {
        grok {
            match => { "message" => "KYPO_BASH_ACTION_AUDIT --- %{GREEDYDATA:auditmessage}" }
        }
        json {
            source => "auditmessage"
            target => "message"
        }
    }
    json {
        source => "message"
        target => "message"
    }
    mutate {
        add_field => { "[@metadata][poolid]" => "%{[message][pool_id]}" }
        add_field => { "[@metadata][sandboxid]" => "%{[message][sandbox_id]}" }
    }
    if("bash-command" in [message][cmd_type]){
        # retrieve elements from json to compose Elasticsearch index correctly
        mutate {
            add_field => { "[@metadata][source_app_identifier]" => "bash.command" }
        }
    }
    if("msf-command" in [message][cmd_type]){
        # retrieve elements from json to compose Elasticsearch index correctly
        mutate {
            add_field => { "[@metadata][source_app_identifier]" => "msf.command" }
        }
    }
    # here, we eliminate top-level field message and set all nested fields of the message as top-level fields
    ruby {
        code => "
            begin
                message= event.get('message')
                if message!= NIL
                    message.keys.each{|key| 
                        event.set(key, message[key]) 
                    }
                    event.remove('message')
                end
            end
        "
    }
    # remove command line trailing white spaces
    mutate {
        strip => ["cmd"]
    }
    # remove unnecessary fields
    mutate {
        remove_field => [ "auditmessage", "cmd_tmp", "procid", "facility", "severity", "@version", "@timestamp", "headers", "tags"]
    }
}
output {
    stdout { codec => rubydebug }
    elasticsearch {
        hosts => [ "elasticsearch:9200" ]
        index => "kypo.logs.console.%{[@metadata][source_app_identifier]}.pool=%{[@metadata][poolid]}.sandbox=%{[@metadata][sandboxid]}"
        codec => json
    }
}