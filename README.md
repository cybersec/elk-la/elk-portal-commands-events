# ELK Portal Commands Events

This project instantiates Logstash, ElasticSearch and Kibana at localhost for analysis of shell commands and training events captured in training sessions held at KYPO CRP or Cyber Sandbox Creator.

## Requirements
Tested with (should also work with higher versions):

| Technology | Version |
| :---: | :---: |
| Docker | 2.2.0.5 |
| Docker Engine | 19.03.8 |
| Docker Compose | 1.25.4 |

Tested on the following PC environments:
* Windows 10 Pro, 16 GB RAM, Intel Core i7-8565 CPU
* macOS Big Sur 11.5.2, 16 GB RAM, CPU, Intel Core i5 CPU, 4 GB RAM set for Resources in Docker Desktop

To install and configure Docker on Windows please go through the following documentation [Docker Desktop](https://docs.docker.com/docker-for-windows/install/).

### Run and Configure ELK
This script will run and configure ELK.
```
$ docker-compose up
```

Or in the case that you do not want to waste the command line use (NOTE: this case is more difficult for debugging purposes since you won't see easily if any error occurs):
```
$ docker-compose up -d
```

On the successful startup (in approximately 30 seconds, in the case, that you have already downloaded the Elasticsearch, Logstash, Kibana images) you should be able to access those pages in the browser `http://localhost:9200/` and `http://localhost:5601/`:

## Library Usage
Just place the data from selected KYPO event (portal events and bash actions), e.g., `~/Echo2019 - Kobylka 3302/2020-05-06 PA197 seminar`. Then go to the folder with `insert-events.sh` file and run the `insert-events.sh` script as follows (please read the next instructions about file naming convention for audit events):
```
$ ./insert-events.sh "PATH-TO-YOUR-KYPO-EVENT-FOLDER"
```

e.g.,

```
$ ./insert-events.sh "~/Echo2019 - Kobylka 3302/2020-05-06 PA197 seminar"
```

***NOTE*** In the case of success, it will print `ok` for each line that was successfully inserted into Elasticsearch.

Where the data directory of, e.g., `~/2020-05-06 PA197 seminar` should be structured as follows:

![kypo-events-folder](https://gitlab.fi.muni.cz/kypolab/elk-portal-events/-/wikis/uploads/a7ffbfdab1d257709f4d54b965fea770/image.png)

on the root level the `training_definition-id*.json` have to be placed and these two folders: (i) `command histories` and (ii) `training events`.

where directory `command histories` (or `command_histories` or `logs`)is, e.g., as follows:

![kypo-bash-history-logs-folder](https://gitlab.fi.muni.cz/kypolab/elk-portal-events/-/wikis/uploads/841b46098f7b1d42a2b6812b0148875a/image.png)

and `training events` (or `training_events`), e.g., is as follows:

![kypo-events-folder](https://gitlab.fi.muni.cz/kypolab/elk-portal-events/-/wikis/uploads/fbebce89ba112084a6ccd9989440f51f/echo_Kobylka_content.PNG)

This script basically goes through all the files in the given directories and inside it search for files that match the pattern `*-events.json` or `*-useractions.json` and inserts all the content of those files under the correct index into local Elasticsearch instance running on port 9200.

## Checking the Data in ELK

Visit Kibana user interface ran from the previous step on the page `http://localhost:5601/`. 

You should see page like this:

![kibana-homepage](https://gitlab.fi.muni.cz/kypolab/elk-portal-events/-/wikis/uploads/9c4811b61e9a7144d3f2958fc8d49fcb/kibana-homepage.PNG)

Here, move to `Dev Tools`,

![kibana-devtools](https://gitlab.fi.muni.cz/kypolab/elk-portal-events/-/wikis/uploads/1d9f5afd2ca60784d0494204f9a1a9da/kibana-devtools.PNG)

and add the following command to the `Console`


```
GET kypo*/_search
{
  "size": 10000,
  "query": {
    "match_all": {}
  }
}
```

![kibana-devtools-example](https://gitlab.fi.muni.cz/kypolab/elk-portal-events/-/wikis/uploads/df2930cdefda1429892a78425c796228/kibana-devtools-example.PNG)

and you should see the similar result as is shown above.


## License

The software was developed by Pavel Seda (seda@fi.muni.cz) and released under the [MIT license](https://gitlab.fi.muni.cz/kypolab/elk-portal-commands-events/-/edit/master/LICENSE).
