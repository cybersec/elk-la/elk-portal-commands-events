#!/bin/bash

ELASTICSEARCH_API_ROOT="http://localhost:9200/"
LOGSTASH_API_ROOT_EVENTS="http://localhost:9602"
LOGSTASH_API_ROOT_BASH="http://localhost:9603"
LOGSTASH_API_ROOT_TD="http://localhost:9604"
TEMPLATE_INFO="_template/template_1"
TEMPLATE_PATH="template.json"

JSON_VALIDATION_ARG="--skip-invalid-json"
DELAYED="--delayed-curl"


usage() {
    N=`basename $0`
    echo "Usage: $N [$JSON_VALIDATION_ARG] path_to_game_data [$DELAYED]"
    echo "       $JSON_VALIDATION_ARG: Skip logs with invalid JSON format"
    echo "       $DELAYED: Each curl is followed by a 1-second delay to avoid duplication of records"
}

check_json_syntax() {
  error=$(echo "$1" | python -mjson.tool 2>&1 1>/dev/null)
  #error=$(python -mjson.tool "$1" 2>&1 1>/dev/null)
  if [ -z "$error" ]; then
    return 0
  else
    echo
    echo "-- SKIPPING LOG EVENT FROM $2: $error"
    echo "-- SKIPPED RECORD: $1"
    return 1
  fi
}

#
# Process input arguments
#
if [[ "$#" -eq 0 || "$#" -gt 3 ]]; then
  usage
  exit 2
fi

if [[ "$#" -eq 1 ]]; then
  DATA_DIR=$1
  ARG=
else
  DATA_DIR=$2
  ARG=$1
fi

if [[ ! -d "$DATA_DIR" ]]; then
  usage
  exit 2
fi

if [[ -n "$ARG" && "$ARG" != "$JSON_VALIDATION_ARG" ]]; then
  usage
  exit 2
fi

if [[ "$#" -eq 3 && "$3" != "$DELAYED" ]]; then
  usage
  exit 2
fi

DELAYED_CURL=false
if [[ "$#" -eq 3 && "$3" == "$DELAYED" ]]; then
  DELAYED_CURL=true
fi

#
# Set template
#
echo
echo "SETTING TEMPLATE..."
curl -H 'Content-Type: application/json' -X PUT -d @${TEMPLATE_PATH} ${ELASTICSEARCH_API_ROOT}${TEMPLATE_INFO}?include_type_name=true
echo

#
# POST data to Elasticsearch
#
pushd "$DATA_DIR"

  ## insert training definition
  echo
  echo "INSERTING TRAINING DEFINITION..."
  for FILE in training_definition-id*.json
  do
    TD_FILE_CONTENT=`cat $FILE`
    curl -X POST -d "$TD_FILE_CONTENT" "${LOGSTASH_API_ROOT_TD}" -H 'Content-Type: application/json'
    if [[ "$DELAYED_CURL" == true ]]; then
      sleep 1
    fi
done
  echo

  ## insert training events
  echo
  echo "INSERTING TRAINING EVENTS..."
  if [ -d "training events" ]; then
    DIR="training events"
  elif [ -d "training_events" ]; then
    DIR="training_events"
  else
    DIR="NONE"
  fi

  if [ -d "$DIR" ]; then
    pushd "$DIR"
    for FILE in *-events.json
    do
      while read -r LINE || [ -n "$LINE" ];
      do
	LINE=$(echo "$LINE" | sed 's/\\\"//g' | sed 's/\\//g' | sed 's/"\[/\[/g' | sed 's/\]"/\]/g')
	if [[ "$ARG" = "$JSON_VALIDATION_ARG" ]]; then
	  check_json_syntax "${LINE}" "${FILE}"
	  if [[ $? -ne 0 ]] ; then
	     continue;
	  fi
	fi
	curl -X POST -d "$LINE" "${LOGSTASH_API_ROOT_EVENTS}" -H 'Content-Type: application/json'
  if [[ "$DELAYED_CURL" == true ]]; then
    sleep 1
  fi
	done < $FILE
    done
    echo
    popd
  fi

  ## insert bash history commands
  echo
  echo "INSERTING BASH HISTORY..."
  if [ -d "command histories" ]; then
    DIR="command histories"
  elif [ -d "command_histories" ]; then
    DIR="command_histories"
  elif [ -d "logs" ]; then
    DIR="logs"
  else
    DIR="NONE"
  fi

  if [ -d "$DIR" ]; then
    pushd "$DIR"
    for FILE in *-useractions.json
    do
      while read -r LINE || [ -n "$LINE" ];
      do
	LINE=$(echo "$LINE")
	if [[ "$ARG" = "$JSON_VALIDATION_ARG" ]]; then
	  check_json_syntax "${LINE}" "${FILE}"
	  if [[ $? -ne 0 ]] ; then
	     continue;
	   fi
	fi
	curl -X POST -d "$LINE" "${LOGSTASH_API_ROOT_BASH}"
 	if [[ "$DELAYED_CURL" == true ]]; then
    sleep 1
  fi
done < $FILE
    done
    echo
    popd
  fi
popd

